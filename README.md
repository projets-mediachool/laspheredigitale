
# Projet Site Web Monde du Digital

## Objectif du Projet

Ce projet vise à créer un site web explorant le monde du digital. Il combine des pages statiques informatives et une section blog dynamique divisée en catégories spécifiques. L'objectif est de fournir une expérience utilisateur fluide, permettant une navigation aisée entre les sections informatives et le blog.

## Détails du Projet

- **Durée du Projet :** 5 jours.
- **Taille des Groupes :** 3 étudiants par groupe.
- **Technologies Utilisées :**
  - HTML/CSS pour la structure et le style.
  - JavaScript pour les interactions dynamiques côté client.
  - PHP pour la logique côté serveur et la gestion du contenu dynamique.
  - SQL pour la base de données du blog.

## Structure et Contenu

### Page d'Accueil

- En-tête avec logo, titre, et navigation principale.
- Slider dynamique.
- Introduction au site.
- Accès aux catégories du blog.
- Section "À propos de Nous".
- Témoignages et Footer.

### Pages Statiques

- **À propos :** Mission, vision, équipe.
- **Contact :** Formulaire de contact, informations de contact, emplacement sur une carte.
- Mentions légales et Politiques de confidentialité.

### Blog avec Catégories

- Technologies Émergentes, Développement Web, Design et UX, Marketing Digital, et Analyses de Cas.
- Chaque catégorie inclut une page de catégorie et des pages d'article.

## Fonctionnalités Spécifiques

- Pages statiques informatives.
- Section blog avec abonnement aux notifications de nouveaux articles.
- Back office pour la gestion des articles, catégories, et commentaires.

## Objectif Additionnel : Charte Graphique

- **Logo :** Design reflétant le thème du monde du digital.
- **Charte Graphique :** Définition d'une palette de couleurs, typographie, éléments visuels, et règles d'utilisation pour assurer une cohérence visuelle.

## Application de la Charte Graphique

- Utilisation de la charte graphique à travers le site web, contenu du blog, et matériaux promotionnels.

## Livrables

- **URL du Site Web :** Le site doit être accessible en ligne.
- **Dossier du Site :** Contenant tous les fichiers du projet.

## Comment Démarrer

> *Ici, vous pouvez inclure des instructions spécifiques sur comment installer, configurer, et exécuter votre projet, ainsi que tout autre détail pertinent pour les personnes souhaitant utiliser ou contribuer au projet.*

## Contribution

Nous encourageons la contribution à notre projet. Veuillez consulter notre fichier CONTRIBUTING.md pour plus d'informations sur comment participer.

## Licence

Ce projet est sous licence XYZ. Voir le fichier [LICENSE.md](LICENSE.md) pour plus de détails.

## Auteurs

- Étudiant 1
- Étudiant 2
- Étudiant 3
