<h1> Me créer un compte </h1>

<form method="POST">
    <input type="text" name="user_name" placeholder="Votre nom" required>
    <input type="text" name="user_surname" placeholder="Votre prénom" required>
    <input type="email" name="email" placeholder="Votre addresse Email" required>
    <input type="password" name="password" placeholder="Définisser votre mot de passe" required>
    <input type="submit" name="create" value="Créer mon compte">
</form>

<?php 

session_start();

include_once("config.php");

if(isset($_POST['create'])){

$user_name=htmlspecialchars($_POST['user_name']);
$user_surname=htmlspecialchars($_POST['user_surname']);
$email=htmlspecialchars($_POST['email']);
$password=password_hash($_POST['password'], PASSWORD_DEFAULT);

$requete=$pdo->prepare("INSERT INTO utilisateur(user_name, user_surname, password, email) VALUES(:user_name, :user_surname, :password, :email)");

$requete->bindParam(":user_name",$user_name);
$requete->bindParam(":user_surname",$user_surname);
$requete->bindParam(":password",$password);
$requete->bindParam(":email",$email);

$requete->execute();

// Définir la session de l'utilisateur
$_SESSION['user_name'] = $user_name;

header('Location:');
exit();

} elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {

    echo "Erreur lors de la création de votre compte, si le problème persiste veuillez réessayer ultérieurement";    
}

?>