<?php
session_start();
    // Vérifiez si l'utilisateur est connecté
    if (isset($_SESSION['user_name'])) {
        $utilisateur_connecte = true;
    } else {
        $utilisateur_connecte = false;
    }
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LSD | connexion</title>
    <link rel="icon" type="svg" href="../img/logo.svg">
    <link rel="stylesheet" href="../css/connexion.css">
    <link rel="stylesheet" href="../css/footer.css">
    <link rel="stylesheet" href="../css/index.css">
    <link rel="stylesheet" href="https://use.typekit.net/lmh7ctl.css">
    <link rel="stylesheet" href="https://use.typekit.net/lmh7ctl.css">
    <script src="../js/script_index.js" defer></script>
    <script src="../js/script_connexion.js" defer></script>
</head>
<body>
    <header>
        <section class="header" id="myHeader">
            <div class="logo">
                <img src="../img/logo_header.svg" alt="image du logo du site">
            </div>
            <nav>
                <ul>
                    <li><a href="../index.php">Accueil</a></li>
                    <li><a href="a_propos.php">A propos</a></li>
                    <li id="blog"><a href="blog.php">Blog</a></li>
                    <li><a href="contact.php">Contact</a></li>
                </ul>
            </nav>
            <div class="CTA">
                <div class="icone">
                    <img src="../img/mail.svg" alt="logo mail">
                </div>
                <div class="icone">
                    <img id="connexionImage"src="../img/login.svg" alt="logo connexion">
                    </div>
<!-- Implentation code PHP -->
<?php
    include_once("config.php");

    $requete = $pdo->prepare("SELECT user_name, admin FROM utilisateur WHERE 1");
    $requete->execute();
    $resultat = $requete->fetch(PDO::FETCH_ASSOC);

    if(isset($_SESSION['user_name']) && $resultat['admin'] == 0) {
        echo '<a href="deco.php" id="deconnexionLink">Déconnexion</a>';
    } elseif(isset($_SESSION['user_name']) && $resultat['admin'] == 1) {
        echo '<a href="admin.php" id="deconnexionLink">Pannel Admin</a>';
        echo '<a href="deco.php" id="deconnexionLink">Déconnexion</a>';
    } elseif(!isset($_SESSION['user_name'])) {
        echo '<a href="connexion.php" id="connexionLink">Connexion</a>';
    }
?>

</div>
        </section>
        <section id="sousMenu">
            <div class="sousMenu">
 <!-- Implentation code PHP -->
                <?php
$requete=$pdo->prepare("SELECT nom FROM categories WHERE 1");
$requete->execute();
$categories=$requete->fetchAll(PDO::FETCH_ASSOC);

foreach($categories as $nom){
                echo'<ul>
                    <li><a href="categorie.php?nom='.$nom['nom'].'">' .$nom['nom']. '</a></li>
                </ul>';
            }
                ?>
            </div>
        </section>
    </header>


    
    <main>
    <section id='connexion_container'>
        <div id='connexion'>
            <div class='connexion_container'>
                <div class='titre_connexion'>
                    <div id='connexion_btn'>Connexion</div>
                    <div id='inscription_btn'>Inscription</div>
                </div>
                <form id='formulaire_connexion' method="POST" action="">
                    <input type="text" name="email" class="input_email" placeholder='Email' required>
                    <input type="password" name="password" class="input_mdp" placeholder='Mot de passe' required>
                    <input class="input_connexion" type="submit" value="Connexion" name="connection">
                </form>

                <?php 

        include_once("config.php"); 

        // Vérifier si le formulaire de connexion a été soumis
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['connection'])) {
            // Récupérer les identifiants saisis par l'utilisateur
            $email = htmlspecialchars($_POST['email']);
            $password = $_POST['password'];

            // Vous devez récupérer les identifiants depuis votre base de données
            $requete = $pdo->prepare("SELECT user_name, password, admin FROM utilisateur WHERE email = :email");
            $requete->bindParam(":email", $email);
            $requete->execute();
            $resultat = $requete->fetch(PDO::FETCH_ASSOC);

        // Vérifier si l'utilisateur existe dans la base de données et si le mot de passe est correct
            if ($resultat && password_verify($password, $resultat['password'])) {
                // Stocker l'identifiant de l'utilisateur dans la session
                $_SESSION['user_name'] = $resultat['user_name'];

                if ($resultat['admin'] == 0){
                header('Location:../index.php'); //envoi vers page accueille

                }elseif(($resultat['admin'] == 1)){
                header('Location:admin.php'); //envoi vers page admin
                }

            } else {
                echo "Identifiants incorrects";
            }
        }   
?>
                <div class='mdp_oublie'><a href="mdp.php">Mot de passe oublié ?</a></div>
            </div>
        </div>
        <div id='inscription'>
            <div class='inscription_container'>
                <div class='titre_connexion'>
                    <div class='connexion_btn'>Connexion</div>
                    <div class='inscription_btn'>Inscription</div>
                </div>
                <form id='formulaire_inscription' method="POST" action="">
                    <input type="text" name="user_name" placeholder='Nom utilisateur' required>
                    <input type="text" name="user_surname" placeholder='Prénom utilisateur' required>
                    <input type="email" name="email" placeholder='E-mail' required>
                    <input type="password" name="password" placeholder='Mot de passe' required>
                    <input type="submit" name="create" value="Inscription">
                </form>
                <?php include_once("config.php");

                    if(isset($_POST['create'])){

                    $user_name=htmlspecialchars($_POST['user_name']);
                    $user_surname=htmlspecialchars($_POST['user_surname']);
                    $email=htmlspecialchars($_POST['email']);
                    $password=password_hash($_POST['password'], PASSWORD_DEFAULT);

                    $requete=$pdo->prepare("INSERT INTO utilisateur(user_name, user_surname, password, email) VALUES(:user_name, :user_surname, :password, :email)");

                    $requete->bindParam(":user_name",$user_name);
                    $requete->bindParam(":user_surname",$user_surname);
                    $requete->bindParam(":password",$password);
                    $requete->bindParam(":email",$email);

                    $requete->execute();

                    // Définir la session de l'utilisateur
                    $_SESSION['user_name'] = $user_name;

                    header('Location:../index.php');
                    exit();

                    } elseif ($_SERVER['REQUEST_METHOD'] === 'POST') {

                        echo "Erreur lors de la création de votre compte, si le problème persiste veuillez réessayer ultérieurement";    
                    }

                ?>
            </div>
        </div>
      </section>
    </main>



    <footer class="flex">
        <section id="categorie_footer flex">
            <ul class="nav_footer flex">
                <?php
                    $requete=$pdo->prepare("SELECT nom FROM categories WHERE 1");
                    $requete->execute();
                    $categories=$requete->fetchAll(PDO::FETCH_ASSOC);

                    foreach($categories as $nom){

                    echo'<li class="nav_item_footer flex">
                        <a href="categorie.php?nom='.$nom['nom'].'">' .$nom['nom']. '</a>';
                    }
                ?>
            </ul>
        </section>
        <div class="line container"></div>
        <section class="footer_down flex container">
            <section class="footer_identite flex">
                <img id="logo_footer" src="../img/logo.svg" alt="logo">
                <div class="text_identite flex">
                    <p class="p_id">LA SPHÈRE DIGITAL</p>
                    <div class="mentions flex">
                        <a href="mentions_legales.php">Mentions légales</a>
                        <p>|</p>
                        <a href="politique.php">Politiques de confidentialités</a>
                    </div>
                </div>
            </section>
            <section class="footer_contact flex">
                <div class="reseaux_size">
                    <a href="#"><img src="../img/x_blanc.svg" alt="logo x"></a>
                    <a href="#"><img src="../img/youtube_blanc.svg" alt="logo youtube"></a>
                    <a href="#"><img src="../img/insta_blanc.svg" alt="logo instagram"></a>
                </div>
                <div class="footer_contact_mail flex">
                    <img id="img_mail" src="../img/mail_blanc.svg" alt="icon de mail">
                    <p>ADRESSEPRO@MEDIASCHOOL.COM</p>
                </div>
            </section>
        </section>
    </footer>
</body>
</html>
