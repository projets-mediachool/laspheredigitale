<?php
session_start();
    // Vérifiez si l'utilisateur est connecté
    if (isset($_SESSION['user_name'])) {
        $utilisateur_connecte = true;
    } else {
        $utilisateur_connecte = false;
    }
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LSD | connexion</title>
    <link rel="icon" type="svg" href="../img/logo.svg">
    <link rel="stylesheet" href="../css/articles.css">
    <link rel="stylesheet" href="../css/footer.css">
    <link rel="stylesheet" href="../css/index.css">
    <link rel="stylesheet" href="https://use.typekit.net/lmh7ctl.css">
    <link rel="stylesheet" href="https://use.typekit.net/lmh7ctl.css">
    <script src="../js/script_index.js" defer></script>
    <script src="../js/script_connexion.js" defer></script>
</head>
<body>
    <header>
        <section class="header" id="myHeader">
            <div class="logo">
                <img src="../img/logo_header.svg" alt="image du logo du site">
            </div>
            <nav>
                <ul>
                    <li><a href="../index.php">Accueil</a></li>
                    <li><a href="a_propos.php">A propos</a></li>
                    <li id="blog"><a href="blog.php">Blog</a></li>
                    <li><a href="contact.php">Contact</a></li>
                </ul>
            </nav>
            <div class="CTA">
                <div class="icone">
                    <img src="../img/mail.svg" alt="logo mail">
                </div>
                <div class="icone">
                    <img id="connexionImage"src="../img/login.svg" alt="logo connexion">
                    </div>
<!-- Implentation code PHP -->
<?php
    include_once("config.php");

    $requete = $pdo->prepare("SELECT user_name, admin FROM utilisateur WHERE 1");
    $requete->execute();
    $resultat = $requete->fetch(PDO::FETCH_ASSOC);

    if(isset($_SESSION['user_name']) && $resultat['admin'] == 0) {
        echo '<a href="deco.php" id="deconnexionLink">Déconnexion</a>';
    } elseif(isset($_SESSION['user_name']) && $resultat['admin'] == 1) {
        echo '<a href="admin.php" id="deconnexionLink">Pannel Admin</a>';
        echo '<a href="deco.php" id="deconnexionLink">Déconnexion</a>';
    } elseif(!isset($_SESSION['user_name'])) {
        echo '<a href="connexion.php" id="connexionLink">Connexion</a>';
    }
?>
</div>
        </section>
        <section id="sousMenu">
            <div class="sousMenu">
 <!-- Implentation code PHP -->
                <?php
$requete=$pdo->prepare("SELECT nom FROM categories WHERE 1");
$requete->execute();
$categories=$requete->fetchAll(PDO::FETCH_ASSOC);

foreach($categories as $nom){
                echo'<ul>
                    <li><a href="categorie.php?nom='.$nom['nom'].'">' .$nom['nom']. '</a></li>
                </ul>';
            }
                ?>
            </div>
        </section>
    </header>
            
    <main>
        <!-- Implentation code PHP -->
        <?php

$title=$_GET['title'];
$requete=$pdo->prepare("SELECT title, chemin, description_longue, categorie_id, created_at, time FROM articles WHERE title=:title");
$requete->bindParam(":title", $title);
$requete->execute();
$result=$requete->fetchAll(PDO::FETCH_ASSOC);

foreach($result as $row){

        echo'<section class="article_container">
            <div id="img_article">
                <img src="'.$row['chemin'].'" alt="image associé à l\'article">
            </div>
            <div id="text_article">
                <div class="article_titre">
                    <div class="nom_categorie">'.$row['categorie_id'].'</div>
                    <h2>'.$row['title'].'</h2>
                    <div class="detail_article">
                        <div>'.$row['created_at'].'</div>
                        <div>|</div>
                        <div> '.$row['time'].' min</div>
                    </div>
                    <div class="description_article">
                        <p>'.$row['description_longue'].'</p>
                    </div>
                </div>
            </div>
        </section>';
        }
    ?>
    </main>  
    <footer class="flex">
        <section id="categorie_footer flex">
            <ul class="nav_footer flex">

            <?php
$requete=$pdo->prepare("SELECT nom FROM categories WHERE 1");
$requete->execute();
$categories=$requete->fetchAll(PDO::FETCH_ASSOC);

foreach($categories as $nom){

                echo'<li class="nav_item_footer flex">
                    <a href="categorie.php?nom='.$nom['nom'].'">' .$nom['nom']. '</a>';
                }
            ?>

            </ul>
        </section>
        <div class="line container"></div>
        <section class="footer_down flex container">
            <section class="footer_identite flex">
                <img id="logo_footer" src="../img/logo.svg" alt="logo">
                <div class="text_identite flex">
                    <p class="p_id">LA SPHÈRE DIGITAL</p>
                    <div class="mentions flex">
                        <a href="mentions_legales.php">Mentions légales</a>
                        <p>|</p>
                        <a href="politique.php">Politiques de confidentialités</a>
                    </div>
                </div>
            </section>
            <section class="footer_contact flex">
                <div class="reseaux_size">
                    <a href="#"><img src="../img/x_blanc.svg" alt="logo x"></a>
                    <a href="#"><img src="../img/youtube_blanc.svg" alt="logo youtube"></a>
                    <a href="#"><img src="../img/insta_blanc.svg" alt="logo instagram"></a>
                </div>
                <div class="footer_contact_mail flex">
                    <img id="img_mail" src="../img/mail_blanc.svg" alt="icon de mail">
                    <p>ADRESSEPRO@MEDIASCHOOL.COM</p>
                </div>
            </section>
        </section>
    </footer>
</body>
</html>