<style>
body {
    font-family: Arial, sans-serif;
    background-color: #f5f5f7; /* Gris clair */
    color: #37373a; /* Gris */
    margin: 0;
    padding: 0;
}

header {
    background-color: #fff; /* Blanc */
    padding: 20px;
    box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1); /* Ombre */
    margin-bottom: 20px; /* Espacement avec le contenu suivant */
}

h1 {
    color: #0077ed; /* Bleu */
    margin: 0; /* Suppression de la marge */
}

form {
    margin-top: 20px;
    margin-bottom: 20px; /* Espacement entre le formulaire et le contenu suivant */
}

label {
    font-weight: bold;
    display: block; /* Afficher les labels sur une ligne différente */
    margin-bottom: 5px; /* Espacement entre les labels */
}

input[type="file"],
input[type="text"],
input[type="number"],
input[type="date"],
textarea {
    width: 100%;
    padding: 8px;
    margin-bottom: 10px;
    border: 1px solid #ccc;
    border-radius: 5px;
    box-sizing: border-box; /* Prendre en compte la taille de la bordure dans la largeur */
}

textarea {
    height: 100px; /* Hauteur fixe pour les zones de texte */
}

select {
    width: 100%;
    padding: 8px;
    margin-bottom: 10px;
    border: 1px solid #ccc;
    border-radius: 5px;
    box-sizing: border-box; /* Prendre en compte la taille de la bordure dans la largeur */
    appearance: none; /* Suppression de l'apparence par défaut */
    background-image: url('data:image/svg+xml;utf8,<svg fill="#000000" height="24" viewBox="0 0 24 24" width="24" xmlns="http://www.w3.org/2000/svg"><path d="M7 10l5 5 5-5z"/><path d="M0 0h24v24H0z" fill="none"/></svg>'); /* Ajout d'une flèche personnalisée */
    background-repeat: no-repeat;
    background-position: right 8px center; /* Position de l'image à droite */
}

input[type="submit"] {
    background-color: #0077ed; /* Bleu */
    color: #fff; /* Blanc */
    padding: 10px 20px;
    border: none;
    border-radius: 5px;
    cursor: pointer;
}

input[type="submit"]:hover {
    background-color: #0056b3; /* Bleu foncé au survol */
}
</style>


<header><h1>Modification article</h1></header>

<?php

include_once("config.php");

$post_id = $_GET['post_id'];

$requete = $pdo->prepare("SELECT chemin, categorie_id, title, author_id, time, created_at, description_courte, description_longue FROM articles WHERE post_id=:post_id");

$requete->bindParam(":post_id", $post_id);

$requete->execute();

$resultat = $requete->fetch(PDO::FETCH_ASSOC);

if ($resultat) {
    echo '<form method="POST" enctype="multipart/form-data">
        <div class="formulaire">
            <label for="chemin">Chemin de votre nouvelle image pour votre article</label><br>
            <input type="file" name="chemin" accept="image/*"><br>';

    $nomselection = $_GET['nom'];
    $requete = $pdo->prepare("SELECT nom FROM categories WHERE 1");
    $requete->execute();
    $nom = $requete->fetchAll(PDO::FETCH_ASSOC); 

    echo'<label for="categorie_id">Nom de la catégorie</label>';

    echo '<select name="categorie_id">';

    if ($nom) {
        foreach ($nom as $caca) {
            $selected = ($caca['nom'] == $resultat['categorie_id']) ? 'selected' : '';
            echo '<option value="'.$caca['nom'].'" '.$selected.'>'.$caca['nom'].'</option>'; 
        }
    }
    
      
        
    echo '</select>';

    echo '<br><label for="title">Modifier le titre de votre article</label><br>
            <input type="text" name="title" value="' . $resultat['title'] . '" required ><br>

            <label for="author_id">Modifier Auteur Article</label><br>
            <input type="text" name="author_id" value="' . $resultat['author_id'] . '" required ><br>

            <label for="time">Durée de lecture (min) </label><br>
            <input type="number" name="time" value="' . $resultat['time'] . '" required ><br>

            <label for="created_at">Écrit le </label><br>
            <input type="date" name="created_at" value="' . $resultat['created_at'] . '" required ><br>

            <label for="description_courte">Description courte Article</label><br>
            <textarea name="description_courte" required >' . $resultat['description_courte'] . '</textarea><br>

            <label for="description_longue">Description longue Article</label><br>
            <textarea name="description_longue" required >' . $resultat['description_longue'] . '</textarea><br>

        </div>
        <input type="submit" name="submit" value="Modifier">
    </form>
    <section>
    <a href="http://localhost/lsd2/apps/admin.php"><button>Retour Accueil Admin</button></a>';
}

if (isset($_POST['submit'])) {

    $post_id = $_GET['post_id'];
    $nom = $_GET['nom'];
    $categorie_id = htmlspecialchars($_POST['categorie_id']);
    $title = htmlspecialchars($_POST['title']);
    $author_id = htmlspecialchars($_POST['author_id']);
    $time = $_POST['time'];
    $created_at = $_POST['created_at'];
    $description_courte = htmlspecialchars($_POST['description_courte']);
    $description_longue = htmlspecialchars($_POST['description_longue']);

    // Gestion du fichier image
    if ($_FILES['chemin']['name'] != '') {
        $target_dir = "uploads/";
        $target_file = $target_dir . basename($_FILES["chemin"]["name"]);
        move_uploaded_file($_FILES["chemin"]["tmp_name"], $target_file);
    } else {
        $target_file = $resultat['chemin']; // Garder le chemin de l'image existante
    }

    $requete = $pdo->prepare("UPDATE articles SET chemin=:chemin, categorie_id=:categorie_id, title=:title, author_id=:author_id, time=:time, created_at=:created_at, description_courte=:description_courte, description_longue=:description_longue WHERE post_id=:post_id");

    $requete->bindParam(':chemin', $target_file);
    $requete->bindParam(':categorie_id', $categorie_id);
    $requete->bindParam(':title', $title);
    $requete->bindParam(':author_id', $author_id);
    $requete->bindParam(':time', $time);
    $requete->bindParam(':created_at', $created_at);
    $requete->bindParam(':description_courte', $description_courte);
    $requete->bindParam(':description_longue', $description_longue);
    $requete->bindParam(':post_id', $post_id);

    $requete->execute();

    header('Location:http://localhost/lsd2/apps/admin.php');
}
?>
