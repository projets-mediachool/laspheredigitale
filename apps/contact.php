<?php
session_start();
    // Vérifiez si l'utilisateur est connecté
    if (isset($_SESSION['user_name'])) {
        $utilisateur_connecte = true;
    } else {
        $utilisateur_connecte = false;
    }
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LSD | Contact</title>
    <link rel="icon" type="svg" href="../img/logo.svg">
    <link rel="stylesheet" href="../css/contact.css">
    <link rel="stylesheet" href="../css/footer.css">
    <link rel="stylesheet" href="../css/index.css">
    <link rel="stylesheet" href="https://use.typekit.net/lmh7ctl.css">
    <link rel="stylesheet" href="https://use.typekit.net/lmh7ctl.css">
    <script src="../js/script_index.js" defer></script>
    <script src="../js/script_connexion.js" defer></script>
</head>
<body>
    <header>
        <section class="header" id="myHeader">
            <div class="logo">
                <img src="../img/logo_header.svg" alt="image du logo du site">
            </div>
            <nav>
                <ul>
                    <li><a href="../index.php">Accueil</a></li>
                    <li><a href="a_propos.php">A propos</a></li>
                    <li id="blog"><a href="blog.php">Blog</a></li>
                    <li><a href="contact.php">Contact</a></li>
                </ul>
            </nav>
            <div class="CTA">
                <div class="icone">
                    <img id="connexionImage"src="../img/login.svg" alt="logo connexion">
                    </div>
            <!-- Implentation code PHP -->
            <?php
                include_once("config.php");

                $requete = $pdo->prepare("SELECT user_name, admin FROM utilisateur WHERE 1");
                $requete->execute();
                $resultat = $requete->fetch(PDO::FETCH_ASSOC);

                if(isset($_SESSION['user_name']) && $resultat['admin'] == 0) {
                    echo '<a href="deco.php" id="deconnexionLink">Déconnexion</a>';
                } elseif(isset($_SESSION['user_name']) && $resultat['admin'] == 1) {
                    echo '<a href="admin.php" id="deconnexionLink">Pannel Admin</a>';
                    echo '<a href="deco.php" id="deconnexionLink">Déconnexion</a>';
                } elseif(!isset($_SESSION['user_name'])) {
                    echo '<a href="connexion.php" id="connexionLink">Connexion</a>';
                }
            ?>

            </div>
        </section>
        <section id="sousMenu">
            <div class="sousMenu">
            <!-- Implentation code PHP -->
                            <?php
            $requete=$pdo->prepare("SELECT nom FROM categories WHERE 1");
            $requete->execute();
            $categories=$requete->fetchAll(PDO::FETCH_ASSOC);

            foreach($categories as $nom){
                echo'<ul>
                    <li><a href="categorie.php?nom='.$nom['nom'].'">' .$nom['nom']. '</a></li>
                </ul>';
            }
                ?>
            </div>
        </section>
    </header>
    <main class="main-container">
        <h1>Contact</h1>
        <div class="contact-info">
            <div class="info-block flex">
                <h2>Adresse</h2>
                <p>2 rue vigier de la pile</p>
                <a href="https://www.google.com/maps/place//data=!4m2!3m1!1s0x47fe33b1fd2f2e9f:0x1b5bb76ebe71cd1a?sa=X&ved=1t:8290&ictx=111" class="info-link">Voir sur la carte →</a>
                <div id='map'><img src="../img/map_contact.svg" alt="logo maps"></div>
            </div>
            <div class="info-block flex">
                <h2>Téléphone</h2>
                <p>01 23 45 67 89</p>
                <a href="tel:0123456789" class="info-link">Appeler →</a>
                <div id='tel'><img src="../img/tel_contact.svg" alt="logo maps"></div>
            </div>
            <div class="info-block flex">
                <h2>Email</h2>
                <p>contact@mediaschool-angouleme.com</p>
                <a href="mailto:contact@example.com" class="info-link">Envoyer un mail →</a>
                <div id='mail'><img src="../img/contact_mail.svg" alt="logo maps"></div>
            </div>
        </div>
        <div class="form-container">
            <h2>Laissez-nous un message</h2>
            <form id="contact-form">
                <div class="form-row">
                    <div class="form-group">
                        <label for="nom">Nom</label>
                        <input type="text" id="nom" name="nom">
                    </div>
                    <div class="form-group">
                        <label for="prenom">Prénom</label>
                        <input type="text" id="prenom" name="prenom">
                    </div>
                </div>
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" id="email" name="email">
                </div>
                <div class="form-group">
                    <label for="sujet">Sujet</label>
                    <input type="text" id="sujet" name="sujet">
                </div>
                <div class="form-group">
                    <label for="message">Message</label>
                    <textarea id="message" name="message"></textarea>
                </div>
                <button type="submit">Envoyé</button>
            </form>
        </div>
    </main>
    <footer class="flex">
        <section id="categorie_footer flex">
            <ul class="nav_footer flex">
                <?php
                    $requete=$pdo->prepare("SELECT nom FROM categories WHERE 1");
                    $requete->execute();
                    $categories=$requete->fetchAll(PDO::FETCH_ASSOC);

                    foreach($categories as $nom){

                    echo'<li class="nav_item_footer flex">
                        <a href="categorie.php?nom='.$nom['nom'].'">' .$nom['nom']. '</a>';
                    }
                ?>
            </ul>
        </section>
        <div class="line container"></div>
        <section class="footer_down flex container">
            <section class="footer_identite flex">
                <img id="logo_footer" src="../img/logo.svg" alt="logo">
                <div class="text_identite flex">
                    <p class="p_id">LA SPHÈRE DIGITAL</p>
                    <div class="mentions flex">
                        <a href="mentions_legales.php">Mentions légales</a>
                        <p>|</p>
                        <a href="politique.php">Politiques de confidentialités</a>
                    </div>
                </div>
            </section>
            <section class="footer_contact flex">
                <div class="reseaux_size">
                    <a href="#"><img src="../img/x_blanc.svg" alt="logo x"></a>
                    <a href="#"><img src="../img/youtube_blanc.svg" alt="logo youtube"></a>
                    <a href="#"><img src="../img/insta_blanc.svg" alt="logo instagram"></a>
                </div>
                <div class="footer_contact_mail flex">
                    <img id="img_mail" src="../img/mail_blanc.svg" alt="icon de mail">
                    <p>ADRESSEPRO@MEDIASCHOOL.COM</p>
                </div>
            </section>
        </section>
    </footer>
</body>
</html>