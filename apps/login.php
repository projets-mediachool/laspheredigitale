<h1> Se connecter </h1>
        <form method="POST">
            <input type="email" name="email" placeholder="Addresse mail" required>
            <input type="password" name="password" placeholder="Mot de passe" required>
            <a href=""> Oubli de mot de passe </a>
            <input type="submit" name="connection" value="Me connecter">
        </form>
<?php
// Démarrer la session

session_start();

include_once("config.php"); 

        // Vérifier si le formulaire de connexion a été soumis
        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['connection'])) {
            // Récupérer les identifiants saisis par l'utilisateur
            $email = htmlspecialchars($_POST['email']);
            $password = $_POST['password'];

            // Vous devez récupérer les identifiants depuis votre base de données
            $requete = $pdo->prepare("SELECT user_name, password, admin FROM utilisateur WHERE email = :email");
            $requete->bindParam(":email", $email);
            $requete->execute();
            $resultat = $requete->fetch(PDO::FETCH_ASSOC);

        // Vérifier si l'utilisateur existe dans la base de données et si le mot de passe est correct
            if ($resultat && password_verify($password, $resultat['password'])) {
                // Stocker l'identifiant de l'utilisateur dans la session
                $_SESSION['user_name'] = $resultat['user_name'];

                if ($resultat['admin'] == 0){
                header('Location:'); //envoi vers page accueille

                }elseif(($resultat['admin'] == 1)){
                header('Location:http://localhost/lsd2/apps/admin.php'); //envoi vers page admin
                }

            } else {
                echo "Identifiants incorrects";
            }
        }   
?>