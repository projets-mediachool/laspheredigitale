<?php
session_start();
    // Vérifiez si l'utilisateur est connecté
    if (isset($_SESSION['user_name'])) {
        $utilisateur_connecte = true;
    } else {
        $utilisateur_connecte = false;
    }
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LSD | Ajout Article</title>
    <link rel="icon" type="svg" href="../img/logo.svg">
    <link rel="stylesheet" href="../css/ajout_article.css">
    <link rel="stylesheet" href="../css/index.css">
    <link rel="stylesheet" href="https://use.typekit.net/lmh7ctl.css">
    <script src="../js/script_index.js" defer></script>
    <script src="../js/script_connexion.js" defer></script>
</head>
<body>
    <header>
        <section class="header" id="myHeader">
            <div class="logo">
                <img src="../img/logo_header.svg" alt="image du logo du site">
            </div>
            <nav>
                <ul>
                    <li><a href="../index.php">Accueil</a></li>
                    <li id="blog"><a href="blog.php">Blog</a></li>
                </ul>
            </nav>
            <div class="CTA">
                <div class="icone">
                    <img id="connexionImage"src="../img/login.svg" alt="logo connexion">
                    </div>
<!-- Implentation code PHP -->
<?php
    include_once("config.php");

    $requete = $pdo->prepare("SELECT user_name, admin FROM utilisateur WHERE 1");
    $requete->execute();
    $resultat = $requete->fetch(PDO::FETCH_ASSOC);

    if(isset($_SESSION['user_name']) && $resultat['admin'] == 0) {
        echo '<a href="deco.php" id="deconnexionLink">Déconnexion</a>';
    } elseif(isset($_SESSION['user_name']) && $resultat['admin'] == 1) {
        echo '<a href="admin.php" id="deconnexionLink">Pannel Admin</a>';
        echo '<a href="deco.php" id="deconnexionLink">Déconnexion</a>';
    } elseif(!isset($_SESSION['user_name'])) {
        echo '<a href="connexion.php" id="connexionLink">Connexion</a>';
    }
?>

</div>
        </section>
        <section id="sousMenu">
            <div class="sousMenu">
 <!-- Implentation code PHP -->
                <?php
                    $requete=$pdo->prepare("SELECT nom FROM categories WHERE 1");
                    $requete->execute();
                    $categories=$requete->fetchAll(PDO::FETCH_ASSOC);

                foreach($categories as $nom){
                echo'<ul>
                    <li><a href="categorie.php?nom='.$nom['nom'].'">' .$nom['nom']. '</a></li>
                </ul>';
                }
                ?>
            </div>
        </section>
    </header>
    <main>
    <?php

    include('config.php');


    $requete = $pdo->prepare("SELECT user_name, admin FROM utilisateur WHERE 1");
    $requete->execute();
    $resultat = $requete->fetch(PDO::FETCH_ASSOC);

    if (isset($_SESSION['user_name']) && $resultat['admin'] == 1) {

        echo '<section>
        <h1>Ajouter un article</h1>
        <form method="POST" enctype="multipart/form-data">
            <div class=formulaire>
                <label for="chemin">Chemin image de votre article</label><br>
                <input type="file" name="chemin" accept="image/*"><br>';

        $requete = $pdo->prepare("SELECT nom FROM categories WHERE 1");
        $requete->execute();
        $categories = $requete->fetchAll(PDO::FETCH_ASSOC);

        if ($categories) {
    
            echo '<label for="categorie_id">Définir la catégorie de votre article: </label>';
            echo '<select name="categorie_id">';

            foreach ($categories as $row) {
                echo '<option value="' . $row['nom'] . '">' . $row['nom'] . '</option>'; 
            }

            echo '</select>';
        } 

        echo   '<br><label for="title">Titre de votre article</label><br>
                <input type="text" name="title" required ><br>

                <label for="author_id">Auteur Article</label><br>
                <input type="text" name="author_id" required ><br>

                <label for="time">Durée de lecture (min) </label><br>
                <input type="number" name="time" required ><br>

                <label for="created_at">Écrit le </label><br>
                <input type="date" name="created_at" required ><br>

                <label for="description_courte">Description courte Article</label><br>
                <textarea name="description_courte" required ></textarea><br>

                <label for="description_longue">Description longue Article</label><br>
                <textarea name="description_longue" required ></textarea><br>

            </div>
            <input type="submit" name="submit" value="Ajouter">
        </form>
        </section>
        <a href="http://localhost/lsd2/apps/admin.php"><button>Retour Accueil Admin</button><a>';

        if ($_SERVER['REQUEST_METHOD'] === 'POST' && isset($_POST['submit'])) {

            if (isset($_FILES["chemin"]) && $_FILES["chemin"]["error"] == 0) {

                $target_dir = "uploads/"; 
                $target_file = $target_dir . basename($_FILES["chemin"]["name"]);

                if (move_uploaded_file($_FILES["chemin"]["tmp_name"], $target_file)) {

                    $categorie_id = htmlspecialchars($_POST['categorie_id']);
                    $title = htmlspecialchars($_POST['title']);
                    $author_id = htmlspecialchars($_POST['author_id']);
                    $time = $_POST['time'];
                    $created_at = $_POST['created_at'];
                    $description_courte = htmlspecialchars($_POST['description_courte']);
                    $description_longue = htmlspecialchars($_POST['description_longue']);

                    $stmt = $pdo->prepare("INSERT INTO articles (chemin, categorie_id, title, author_id, time, created_at, description_courte, description_longue) VALUES (:chemin, :categorie_id, :title, :author_id, :time, :created_at, :description_courte, :description_longue)");
                    $stmt->bindParam(':chemin', $target_file);
                    $stmt->bindParam(':categorie_id', $categorie_id);
                    $stmt->bindParam(':title', $title);
                    $stmt->bindParam(':author_id', $author_id);
                    $stmt->bindParam(':time', $time);
                    $stmt->bindParam(':created_at', $created_at);
                    $stmt->bindParam(':description_courte', $description_courte);
                    $stmt->bindParam(':description_longue', $description_longue);

                    try {
                        $stmt->execute();
                        header('Location:http:admin.php');
                    } catch(PDOException $e) {
                        echo "Erreur: " . $e->getMessage();
                    }
                } else {
                    echo "Désolé, il y a une erreur lors de l'ajout de votre article.";
                }
            }
        }
    }
    ?>

</main>

