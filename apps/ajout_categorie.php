<?php
session_start();
    // Vérifiez si l'utilisateur est connecté
    if (isset($_SESSION['user_name'])) {
        $utilisateur_connecte = true;
    } else {
        $utilisateur_connecte = false;
    }
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LSD | Ajout Article</title>
    <link rel="icon" type="svg" href="../img/logo.svg">
    <link rel="stylesheet" href="../css/categorieStyle.css">
    <link rel="stylesheet" href="../css/index.css">
    <link rel="stylesheet" href="https://use.typekit.net/lmh7ctl.css">
    <script src="../js/script_index.js" defer></script>
    <script src="../js/script_connexion.js" defer></script>
</head>
<body>
    <header>
        <section class="header" id="myHeader">
            <div class="logo">
                <img src="../img/logo_header.svg" alt="image du logo du site">
            </div>
            <nav>
                <ul>
                    <li><a href="../index.php">Accueil</a></li>
                    <li id="blog"><a href="blog.php">Blog</a></li>
                </ul>
            </nav>
            <div class="CTA">
                <div class="icone">
                    <img id="connexionImage"src="../img/login.svg" alt="logo connexion">
                    </div>
<!-- Implentation code PHP -->
<?php
    include_once("config.php");

    $requete = $pdo->prepare("SELECT user_name, admin FROM utilisateur WHERE 1");
    $requete->execute();
    $resultat = $requete->fetch(PDO::FETCH_ASSOC);

    if(isset($_SESSION['user_name']) && $resultat['admin'] == 0) {
        echo '<a href="deco.php" id="deconnexionLink">Déconnexion</a>';
    } elseif(isset($_SESSION['user_name']) && $resultat['admin'] == 1) {
        echo '<a href="admin.php" id="deconnexionLink">Pannel Admin</a>';
        echo '<a href="deco.php" id="deconnexionLink">Déconnexion</a>';
    } elseif(!isset($_SESSION['user_name'])) {
        echo '<a href="connexion.php" id="connexionLink">Connexion</a>';
    }
?>

</div>
        </section>
        <section id="sousMenu">
            <div class="sousMenu">
 <!-- Implentation code PHP -->
                <?php
                    $requete=$pdo->prepare("SELECT nom FROM categories WHERE 1");
                    $requete->execute();
                    $categories=$requete->fetchAll(PDO::FETCH_ASSOC);

                foreach($categories as $nom){
                echo'<ul>
                    <li><a href="categorie.php?nom='.$nom['nom'].'">' .$nom['nom']. '</a></li>
                </ul>';
                }
                ?>
            </div>
        </section>
    </header>
    
<?php
include('config.php');


$requete = $pdo->prepare("SELECT user_name, admin FROM utilisateur WHERE 1");
$requete->execute();
$resultat = $requete->fetch(PDO::FETCH_ASSOC);

if(isset($_SESSION['user_name']) && $resultat['admin']==1 ){

    echo '<section id="ajout_categorie">
        <h1>Ajouter une catégorie</h1>
        <form method="POST">
            <div class=formulaire>
                <label for="categorie_nom">Nom de la catégorie:</label><br>
                <input type="text" id="categorie_nom" name="categorie_nom" required><br><br>
                <label for="categorie_description">Description de la catégorie:</label><br>
                <textarea id="categorie_description" name="categorie_description" required></textarea><br><br>
            </div>
            <input type="submit" name="ajouter" value="Ajouter">
        </form>
        <div id="retour_accueil">
            <a href="admin.php"><button>Retour Accueil Admin</button><a>
        </div>
    </section>';
    

    if(isset($_POST['ajouter'])){

    $categorie_nom = $_POST['categorie_nom'];
    $categorie_description = $_POST['categorie_description'];

    // Requête préparée pour insérer la nouvelle catégorie avec la description
    $requete = $pdo->prepare("INSERT INTO categories (nom, description) VALUES (:nom, :description)");
    $requete->bindParam(':nom', $categorie_nom);
    $requete->bindParam(':description', $categorie_description);
    $requete->execute();

    header('Location:admin.php');
    }
}
?>

</body>
</html>