<?php
session_start();
// Vérifiez si l'utilisateur est connecté
if (isset($_SESSION['user_name'])) {
    $utilisateur_connecte = true;
} else {
    $utilisateur_connecte = false;
}
?>

<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LSD | Mot de passe oublié</title>
    <link rel="icon" type="svg" href="../img/logo.svg">
    <link rel="stylesheet" href="../css/index.css">
    <link rel="stylesheet" href="../css/footer.css">
    <link rel="stylesheet" href="../css/mdp.css">
    <link rel="stylesheet" href="https://use.typekit.net/lmh7ctl.css">
    <script src="js/script_index.js" defer></script>
    <script src="js/script_newsletter.js" defer></script>
</head>

<body>
    <header>
        <section class="header" id="myHeader">
            <div class="logo">
                <img src="../img/logo_header.svg" alt="image du logo du site">
            </div>
            <nav>
                <ul>
                    <li><a href="../index.php">Accueil</a></li>
                    <li><a href="../apps/a_propos.php">A propos</a></li>
                    <li id="blog"><a href="../apps/blog.php">Blog</a></li>
                    <li><a href="../apps/contact.php">Contact</a></li>
                </ul>
            </nav>
            <div class="CTA">
                <div class="icone">
                    <img id='newsletter' src="../img/mail.svg" alt="logo mail">
                </div>
                <div class="icone">
                    <img id="connexionImage" src="../img/login.svg" alt="logo connexion">
                </div>
                <a href="../apps/connexion.php" id="connexionLink" style="display:none;">Connexion</a>
                <a href="../apps/deco.php" id="deconnexionLink" style="display:none;">Déconnexion</a>
            </div>
        </section>
        <section id="sousMenu">
            <div class="sousMenu">
                <ul>
                    <li><a href="apps/categorie.php">Technologie Emergentes</a></li>
                    <li><a href="">Developpement Web</a></li>
                    <li><a href="">Design UX/UI</a></li>
                    <li><a href="">Marketting Digital</a></li>
                    <li><a href="">Analyse de cas</a></li>
                </ul>
            </div>
        </section>
        <div class="popup" id="popup">
            <h2>Inscrivez-vous à notre newsletter</h2>
            <form>
                <input type="email" placeholder="Entrez votre adresse e-mail">
                <button type="submit">S'abonner</button>
            </form>
            <button id='close_popup' onclick="closePopup()">Fermer</button>
        </div>
        <div class="overlay" id="overlay"></div>
    </header>




    <main>
    <section id='connexion_container'>
        <div id='connexion'>
            <div class='connexion_container'>
                <div class='titre_connexion'>
                    <div id='info_oublie'>Connexion</div>
                    <p>Veuillez saisir votre email de connexion afin de <br> recevoir le lien de réinitialisation de votre mot
                    de passe.</p>
                </div>
                <form id='formulaire_oublie' action="">
                    <input type="text" placeholder='Saisir votre adresse email'>
                    <button>Recevoir le lien</button>
                </form>
                <div class='retour'><a href="connexion.php">Retour a la page de connexion</a></div>
            </div>
        </div>
      </section>
    </main>




    <footer class="flex">
        <section id="categorie_footer flex">
            <ul class="nav_footer flex">
                <li class="nav_item_footer flex">
                    <a href="#">Design & UX</a>
                </li>
                <li class="nav_item_footer flex">
                    <a href="#">Développement Web</a>
                </li>
                <li class="nav_item_footer flex">
                    <a href="#">Technologies Émergentes</a>
                </li>
                <li class="nav_item_footer flex">
                    <a href="#">Marketing digital</a>
                </li>
                <li class="nav_item_footer flex">
                    <a href="#">Analyse de cas</a>
                </li>
            </ul>
        </section>
        <div class="line container"></div>
        <section class="footer_down flex container">
            <section class="footer_identite flex">
                <img id="logo_footer" src="../img/logo.svg" alt="logo">
                <div class="text_identite flex">
                    <p class="p_id">LA SPHÈRE DIGITAL</p>
                    <div class="mentions flex">
                        <a href="../apps/mentions_legales.php">Mentions légales</a>
                        <p>|</p>
                        <a href="../apps/politique.php">Politiques de confidentialités</a>
                    </div>
                </div>
            </section>
            <section class="footer_contact flex">
                <div class="reseaux_size">
                    <a href="#"><img src="../img/x_blanc.svg" alt="logo x"></a>
                    <a href="#"><img src="../img/youtube_blanc.svg" alt="logo youtube"></a>
                    <a href="#"><img src="../img/insta_blanc.svg" alt="logo instagram"></a>
                </div>
                <div class="footer_contact_mail flex">
                    <img id="img_mail" src="../img/mail_blanc.svg" alt="icon de mail">
                    <p>ADRESSEPRO@MEDIASCHOOL.COM</p>
                </div>
            </section>
        </section>
    </footer>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var connexionImage = document.getElementById('connexionImage');
            var connexionLink = document.getElementById('connexionLink');
            var deconnexionLink = document.getElementById('deconnexionLink');

            <?php if ($utilisateur_connecte) : ?>
                connexionImage.addEventListener('click', function() {
                    // Afficher le lien de déconnexion
                    deconnexionLink.style.display = 'inline';
                    // Masquer le lien de connexion
                    connexionLink.style.display = 'none';
                });
            <?php else : ?>
                connexionImage.addEventListener('click', function() {
                    // Afficher le lien de connexion
                    connexionLink.style.display = 'inline';
                    // Masquer le lien de déconnexion
                    deconnexionLink.style.display = 'none';
                });
            <?php endif; ?>
        });
    </script>
</body>

</html>