<?php
include('config.php');

// Vérifier si l'ID de la catégorie est passé en paramètre
if(isset($_GET['nom'])) {
    $nom = $_GET['nom'];

    // Récupérer les détails de la catégorie à partir de l'ID
    $stmt = $pdo->prepare("SELECT categorie_id, nom, description FROM categories WHERE nom = :nom");
    $stmt->bindParam(':nom', $nom);
    $stmt->execute();
    $category = $stmt->fetch(PDO::FETCH_ASSOC);

    if (!$category) {
        echo "Catégorie non trouvée.";
        exit;
    }
} else {
    echo "ID de catégorie non spécifié.";
    exit;
}

// Vérifier si le formulaire est soumis
if ($_SERVER["REQUEST_METHOD"] == "POST" && isset($_POST['submit'])) {
    $nouveau_nom_categorie = htmlspecialchars($_POST['nouveau_nom_categorie']);
    $nouvelle_description_categorie = htmlspecialchars($_POST['nouvelle_description_categorie']);

    $stmt = $pdo->prepare("UPDATE categories SET nom=:nouveau_nom, description=:description WHERE nom=:ancien_nom");
    $stmt->bindParam(':nouveau_nom', $nouveau_nom_categorie);
    $stmt->bindParam(':description', $nouvelle_description_categorie);
    $stmt->bindParam(':ancien_nom', $nom);


    try {
        $stmt->execute();
        header('Location:http://localhost/lsd2/apps/admin.php');
    } catch(PDOException $e) {
        echo "Erreur: " . $e->getMessage();
    }
}
?>
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="ressources/css/categorieStyle.css">
    <link rel="stylesheet" href="https://use.typekit.net/lmh7ctl.css">
    <link rel="stylesheet" href="https://use.typekit.net/lmh7ctl.css">
    <title>Modifier une catégorie</title>
</head>
<body>
    <section>
        <h1>Modifier une catégorie</h1>
        <form method="POST">
            <div class="formulaire">
                <label for="nouveau_nom_categorie">Nouveau nom de la catégorie:</label><br>
                <input type="text" id="nouveau_nom_categorie" name="nouveau_nom_categorie" value="<?php echo $category['nom']; ?>" required><br><br>
                <label for="nouvelle_description_categorie">Nouvelle description de la catégorie:</label><br>
                <textarea id="nouvelle_description_categorie" name="nouvelle_description_categorie" required><?php echo $category['description']; ?></textarea><br><br>
            </div>
            <input type="submit" name="submit" value="Modifier">
        </form>
    </section>
    <a href="http://localhost/lsd2/apps/admin.php"><button>Retour Accueil Admin</button></a>'
</body>
</html>