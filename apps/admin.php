<style>
    body {
        font-family: Arial, sans-serif;
        background-color: #f5f5f7; /* Gris clair */
        color: #37373a; /* Gris */
        margin: 0;
        padding: 0;
        display: flex;
        flex-direction: column;
        align-items: center;
    }
    header {
        width: 100%;
        background-color: #fff; /* Blanc */
        padding: 20px;
        box-shadow: 0px 2px 4px rgba(0, 0, 0, 0.1); /* Ombre */
        margin-bottom: 20px; /* Espacement avec le contenu suivant */
    }
    h1 {
        color: #0077ed; /* Bleu */
        margin: 30px 0 20px; /* Augmentation de la marge top */
    }
    h2{
        font-size: 50px;
        display: flex;
        width: 35%;
        justify-content: flex-end;
    }
    .modif_cate{
        margin:0 38px;
    }
    form {
        margin-top: 20px;
        margin-bottom: 20px; /* Espacement entre le formulaire et le contenu suivant */
    }
    label {
        font-weight: bold;
    }
    select, input[type="submit"] {
        padding: 8px 12px;
        margin-right: 10px;
        border: 1px solid #0077ed; /* Bleu */
        border-radius: 5px;
        background-color: #fff; /* Blanc */
        color: #37373a; /* Gris */
        cursor: pointer;
    }
    input[type="search"] {
        padding: 8px 12px;
        margin-right: 10px;
        border: 1px solid #0077ed; /* Bleu */
        border-radius: 5px;
    }
    input[type="submit"] {
        background-color: #0077ed; /* Bleu */
        color: #fff; /* Blanc */
    }
    input[type="submit"]:hover {
        background-color: #0056b3; /* Bleu foncé au survol */
    }
    .deco{
        background-color:red !important;
        border:0px !important;
    }
    table {
        border-collapse: collapse;
        width: 90%;
        margin-top: 20px;
        border: 2px solid #0077ed; /* Bordure bleue */
        border-radius: 10px; /* Coins arrondis */
        overflow: hidden; /* Empêche le contenu de déborder */
    }
    img{
        max-width: 150px; /* Réduction de la taille des images */
        max-height: 150px;
    }
    th, td {
        border: none; /* Suppression des bordures */
        padding: 8px;
        text-align: left;
        background-color: #f5f5f7; /* Fond gris */
        text-align: center; /* Centrage du contenu */
    }
    th {
        background-color: #0077ed; /* Bleu */
        color: #fff; /* Blanc */
    }
    td a {
        color: #0077ed; /* Bleu */
        text-decoration: none;
        margin-right: 10px;
        font-weight: bold;
    }
    td a:hover {
        text-decoration: underline;
    }
    button {
        padding: 8px 12px;
        border: none;
        border-radius: 5px;
        background-color: #0077ed; /* Bleu */
        color: #fff; /* Blanc */
        cursor: pointer;
    }
    .flex{
        display:flex;
    }
    .pluscate {
        margin-right: 10px; /* Espacement entre les boutons */
    }
    .pluscate, .plusarticle {
        margin-right: 10px; /* Espacement entre les boutons */
        width: 120px;
    }
    button.supprimer {
        background-color: #dc3545; /* Rouge */
    }
    button:hover {
        background-color: #0056b3; /* Bleu foncé au survol */
    }
    .article img {
        max-width: 200px; /* Réduction de la taille des images */
        max-height: 200px;
        display: block;
        margin: 0 auto; /* Centrage des images */
    }
    .buttons-container {
    display: flex;
    justify-content: space-between; /* Pour espacer les boutons */
}
</style>

<?php

include_once("config.php");

// Fonction de déconnexion
function deconnexion() {
    session_unset();
    session_destroy();
}

session_start();

$requete = $pdo->prepare("SELECT user_name, admin FROM utilisateur WHERE 1");
$requete->execute();
$resultat = $requete->fetch(PDO::FETCH_ASSOC);

if(isset($_SESSION['user_name']) && $resultat['admin']==1 ){

    echo'<h1>Page Admin</h1>';
    echo'<form method="POST" action="../apps/login.php">
        <input class="deco" type="submit" name="deco" value="Déconnexion">
        </form>';

    if(isset($_POST['deco'])){
        deconnexion();
    }

    echo'<div class="buttons-container">
    <a href="../apps/ajout_categorie.php"><button class="pluscate">Ajouter une catégorie</button></a>
    <a href="../apps/ajout_article.php"><button class="plusarticle">Ajouter un article</button></a>
</div>';


    $requete=$pdo->prepare("SELECT nom, categorie_id FROM categories WHERE 1");
    $requete->execute();
    $categories = $requete->fetchAll(PDO::FETCH_ASSOC);

    if($categories){

        echo '<form method="POST">'; 
        echo '<label for="categorie">Choisissez la catégorie:</label>';
        echo '<select name="categorie">';

        foreach ($categories as $row) {
        $selected = ($row['nom'] == $_POST['categorie']) ? 'selected' : ''; // Vérification si l'option est sélectionnée
        echo '<option value="' . $row['nom'] . '" ' . $selected . '>' . $row['nom'] . '</option>'; 
        }

        echo '</select>';
        echo '<input type="submit" name="selection" value="Valider">';
        echo '</form>';
    
        if(isset($_POST['selection'])){

            $categorie_nom = $_POST['categorie'];

            echo '<h2>' . $_POST['categorie'] . '
                <div class="modif_cate">
                 <a href="../apps/modifier_categorie.php?nom='.$categorie_nom.'"><button>Modifier la catégorie </button></a> 
                 <a href="../apps/supprimer_categorie.php?nom='.$categorie_nom.'"><button>Supprimer la catégorie</button></a>
                 </div>
                 </h2>'; // Affichage du titre de la catégorie sélectionnée

            $requete = $pdo->prepare("SELECT chemin, title, categorie_id, post_id FROM articles WHERE categorie_id = (SELECT nom FROM categories WHERE nom = :categorie_nom)");
            $requete->bindParam(":categorie_nom", $categorie_nom);
            $requete->execute();

            $result = $requete->fetchAll(PDO::FETCH_ASSOC);

            if($result){

                foreach($result as $article){
                    echo '<table>
                        <tr>
                            <td> <img src="'.$article['chemin'].'"> <td>
                        </tr>
                        <tr> 
                            <td>'. $article['title'] . '</td>
                        </tr>
                        <tr>
                            <td> <a href="../apps/modifier_article.php?post_id='.$article['post_id'].'&nom='.$row['nom'].'"><button>Modifier article</button></a>
                            <a href="../apps/supprimer_article.php?post_id='.$article['post_id'].'"><button class="supprimer">Supprimer article</button></a> </td>
                        </tr>
                        </table>';
                }
            }else{
                echo 'Aucun article trouvé';
                echo '<br>';
            }
        }
    }

}
?>