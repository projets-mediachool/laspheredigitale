<?php
session_start();
// Fonction de déconnexion
function deconnexion() {
    session_unset();
    session_destroy();
}

deconnexion();
header('Location:../index.php');
?>