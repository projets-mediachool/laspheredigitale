<?php
session_start();
    // Vérifiez si l'utilisateur est connecté
    if (isset($_SESSION['user_name'])) {
        $utilisateur_connecte = true;
    } else {
        $utilisateur_connecte = false;
    }
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LSD | connexion</title>
    <link rel="icon" type="svg" href="../img/logo.svg">
    <link rel="stylesheet" href="../css/politique.css">
    <link rel="stylesheet" href="../css/footer.css">
    <link rel="stylesheet" href="../css/index.css">
    <link rel="stylesheet" href="https://use.typekit.net/lmh7ctl.css">
    <link rel="stylesheet" href="https://use.typekit.net/lmh7ctl.css">
    <script src="../js/script_index.js" defer></script>
    <script src="../js/script_connexion.js" defer></script>
</head>
<body>
    <header>
        <section class="header" id="myHeader">
            <div class="logo">
                <img src="../img/logo_header.svg" alt="image du logo du site">
            </div>
            <nav>
                <ul>
                    <li><a href="../index.php">Accueil</a></li>
                    <li><a href="a_propos.php">A propos</a></li>
                    <li id="blog"><a href="blog.php">Blog</a></li>
                    <li><a href="contact.php">Contact</a></li>
                </ul>
            </nav>
            <div class="CTA">
                <div class="icone">
                    <img src="../img/mail.svg" alt="logo mail">
                </div>
                <div class="icone">
                    <img id="connexionImage"src="../img/login.svg" alt="logo connexion">
                    </div>
<!-- Implentation code PHP -->
<?php
    include_once("config.php");

    $requete = $pdo->prepare("SELECT user_name, admin FROM utilisateur WHERE 1");
    $requete->execute();
    $resultat = $requete->fetch(PDO::FETCH_ASSOC);

    if(isset($_SESSION['user_name']) && $resultat['admin'] == 0) {
        echo '<a href="deco.php" id="deconnexionLink">Déconnexion</a>';
    } elseif(isset($_SESSION['user_name']) && $resultat['admin'] == 1) {
        echo '<a href="admin.php" id="deconnexionLink">Pannel Admin</a>';
        echo '<a href="deco.php" id="deconnexionLink">Déconnexion</a>';
    } elseif(!isset($_SESSION['user_name'])) {
        echo '<a href="connexion.php" id="connexionLink">Connexion</a>';
    }
?>

</div>
        </section>
        <section id="sousMenu">
            <div class="sousMenu">
 <!-- Implentation code PHP -->
                <?php
$requete=$pdo->prepare("SELECT nom FROM categories WHERE 1");
$requete->execute();
$categories=$requete->fetchAll(PDO::FETCH_ASSOC);

foreach($categories as $nom){
                echo'<ul>
                    <li><a href="categorie.php?nom='.$nom['nom'].'">' .$nom['nom']. '</a></li>
                </ul>';
            }
                ?>
            </div>
        </section>
    </header>
    <main>
        <h1 id="PDC" class="container">Politique de confidentialité</h1>
        <section class="fond container">
            <div>
                <h2>1. Éditeur du site</h2>
                <p>Bienvenue sur La sphère digitale, votre ressource dédiée à la technologie et au digital. <br>Notre engagement est de protéger et respecter votre vie privée.</p> <br>
            </div>
            <div>
                <h2>2. Collecte des Données Personnelles</h2> <br>
                <p>Nous recueillons des données personnelles lors de l'inscription sur notre site, de la publication <br> de commentaires, ou de la participation dans les forums. Les données collectées incluent :</p> <br>
        
                <p>- Nom et prénom</p>
                <p>- Adresse email</p>
                <p>- Informations de connexion et d'activité sur le site</p> <br>
            </div>
            <div>
                <h2>3. Utilisation des Données</h2> <br>
                <p>Nous utilisons vos données pour :</p> <br>
                <p>- Administrer votre compte <br>
                -Faciliter vos interactions et commentaires <br>
                - Vous envoyer des communications si vous avez opté pour cela</p> <br>
                </p>
            </div>
            <div>
                <h2>4. Consentement</h2> <br>
                <p>En vous inscrivant et en utilisant La sphère digitale, vous consentez à <br> notre politique de confidentialité et autorisez la collecte et <br> l'utilisation de vos informations personnelles.</p> <br>
            </div>
            <div>
                <h2>5. Sécurité des Données</h2> <br>
                <p>Nous nous engageons à protéger vos données contre la perte, l'usage abusif <br> et l'accès non autorisé grâce à des mesures de sécurité appropriées.</p> <br
            </div>
            <div>
                <h2>6. Vos Droits</h2> <br>
                <p>Conformément au RGPD, vous avez le droit à l'accès, à la rectification, à l'effacement et à la limitation du traitement <br> de vos données personnelles. Pour exercer ces droits, veuillez nous contacter à l'adresse suivante : [Adresse Email du Support].</p> <br>
                <p>Si vous souhaitez retirer votre consentement à l'utilisation de vos données personnelles ou demander leur suppression, <br> veuillez envoyer une demande explicite à notre service de support à rgpd@mediaschool.eu.</p> <br>
            </div>
            <div>
                <h2>7. Absence de Cookies</h2> <br>
                <p>Notre site ne fait pas usage de cookies pour tracer ou stocker vos informations de navigation.</p> <br>

            </div>
            <div>
                <h2>8. Partage des Données</h2> <br>
                <p>Vos données personnelles ne seront pas vendues ni louées à des <br> tiers. Elles seront partagées uniquement avec des prestataires de <br> services tiers lorsque cela est nécessaire pour le fonctionnement <br> du site.</p> <br>
            </div>
            <div>
                <h2>9. Modifications de la Politique de Confidentialité</h2> <br>
                <p>Cette politique peut être mise à jour pour refléter les <br> changements dans nos pratiques de confidentialité. Toute <br> modification sera communiquée sur cette page.</p> <br>
                
            </div>
            <div>
                <h2>10. Contact</h2><br>
                <p>Si vous avez des questions sur cette politique ou sur nos pratiques de confidentialité <br>
                , n'hésitez pas à nous contacter à rgpd@mediaschool.eu.</p> <br>
            </div>
            <p>Date d'efficacité : 27 mars 2024</p>
        </section>
    </main>
    <footer class="flex">
        <section id="categorie_footer flex">
            <ul class="nav_footer flex">

            <?php
$requete=$pdo->prepare("SELECT nom FROM categories WHERE 1");
$requete->execute();
$categories=$requete->fetchAll(PDO::FETCH_ASSOC);

foreach($categories as $nom){

                echo'<li class="nav_item_footer flex">
                    <a href="categorie.php?nom='.$nom['nom'].'">' .$nom['nom']. '</a>';
                }
            ?>

            </ul>
        </section>
        <div class="line container"></div>
        <section class="footer_down flex container">
            <section class="footer_identite flex">
                <img id="logo_footer" src="../img/logo.svg" alt="logo">
                <div class="text_identite flex">
                    <p class="p_id">LA SPHÈRE DIGITAL</p>
                    <div class="mentions flex">
                        <a href="mentions_legales.php">Mentions légales</a>
                        <p>|</p>
                        <a href="politique.php">Politiques de confidentialités</a>
                    </div>
                </div>
            </section>
            <section class="footer_contact flex">
                <div class="reseaux_size">
                    <a href="#"><img src="../img/x_blanc.svg" alt="logo x"></a>
                    <a href="#"><img src="../img/youtube_blanc.svg" alt="logo youtube"></a>
                    <a href="#"><img src="../img/insta_blanc.svg" alt="logo instagram"></a>
                </div>
                <div class="footer_contact_mail flex">
                    <img id="img_mail" src="../img/mail_blanc.svg" alt="icon de mail">
                    <p>ADRESSEPRO@MEDIASCHOOL.COM</p>
                </div>
            </section>
        </section>
    </footer>
</body>
</html>