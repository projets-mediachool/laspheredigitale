<?php

include_once("config.php");

function afficherArticles($articles, $pdo) {
    if (!empty($articles)) {
        foreach ($articles as $article) {
            echo '<div class="article">
                    <img src="' . htmlspecialchars($article['chemin']) . '" alt="Image" class="article-image">
                    <h3>' . htmlspecialchars($article['title']) . '</h3>
                    <p>' . htmlspecialchars($article['description_courte']) . '</p>
                    <small>Écrit le ' . htmlspecialchars($article['created_at']) . ' | ' . htmlspecialchars($article['time']) . ' min</small>
                    <a href="">Cliquez ici</a>
                </div>';
        }
    } else {
        $requete = $pdo->prepare("SELECT chemin, title, time, created_at, description_courte FROM articles");
        $requete->execute();
        $tous_articles = $requete->fetchAll(PDO::FETCH_ASSOC);
        echo '<div class="container_article">';
        foreach ($tous_articles as $ligne) {
            echo '<div class="article">
                    <img src="' . htmlspecialchars($ligne['chemin']) . '" alt="Image" class="article-image">
                    <h3>' . htmlspecialchars($ligne['title']) . '</h3>
                    <p>' . htmlspecialchars($ligne['description_courte']) . '</p>
                    <small>Écrit le ' . htmlspecialchars($ligne['created_at']) . ' | ' . htmlspecialchars($ligne['time']) . ' min</small>
                    <a href="">Cliquez ici</a>
                </div>';
        }
        echo '</div>';
    }
}

$articles = [];
$search = isset($_POST['search']) ? $_POST['search'] : '';
$tri = isset($_POST['tri']) ? $_POST['tri'] : 'recent';

if (isset($_POST['search_submit']) || isset($_POST['trier'])) {
    $order = $tri === 'recent' ? 'DESC' : 'ASC';
    $searchParam = '%' . $search . '%';
    $requete = $pdo->prepare("SELECT chemin, title, time, created_at, description_courte FROM articles WHERE title LIKE :search ORDER BY created_at $order");
    $requete->bindParam(':search', $searchParam, PDO::PARAM_STR);
    $requete->execute();
    $articles = $requete->fetchAll(PDO::FETCH_ASSOC);
}
?>




<!-- ========================================================================== -->

<?php
session_start();
    // Vérifiez si l'utilisateur est connecté
    if (isset($_SESSION['user_name'])) {
        $utilisateur_connecte = true;
    } else {
        $utilisateur_connecte = false;
    }
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LSD | connexion</title>
    <link rel="icon" type="svg" href="../img/logo.svg">
    <link rel="stylesheet" href="../css/blog.css">
    <link rel="stylesheet" href="../css/footer.css">
    <link rel="stylesheet" href="../css/index.css">
    <link rel="stylesheet" href="https://use.typekit.net/lmh7ctl.css">
    <link rel="stylesheet" href="https://use.typekit.net/lmh7ctl.css">
    <script src="../js/script_index.js" defer></script>
    <script src="../js/script_connexion.js" defer></script>
    <script src="../js/script_newsletter.js" defer></script>
</head>
<body>
    <header>
        <section class="header" id="myHeader">
            <div class="logo">
                <img src="../img/logo_header.svg" alt="image du logo du site">
            </div>
            <nav>
                <ul>
                    <li><a href="../index.php">Accueil</a></li>
                    <li><a href="a_propos.php">A propos</a></li>
                    <li id="blog"><a href="blog.php">Blog</a></li>
                    <li><a href="contact.php">Contact</a></li>
                </ul>
            </nav>
            <div class="CTA">
                <div class="icone">
                    <img id='newsletter' src="../img/mail.svg" alt="logo mail">
                </div>
                <div class="icone">
                    <img id="connexionImage"src="../img/login.svg" alt="logo connexion">
                    </div>
<!-- Implentation code PHP -->
<?php

    $requete = $pdo->prepare("SELECT user_name, admin FROM utilisateur WHERE 1");
    $requete->execute();
    $resultat = $requete->fetch(PDO::FETCH_ASSOC);

    if(isset($_SESSION['user_name']) && $resultat['admin'] == 0) {
        echo '<a href="deco.php" id="deconnexionLink">Déconnexion</a>';
    } elseif(isset($_SESSION['user_name']) && $resultat['admin'] == 1) {
        echo '<a href="admin.php" id="deconnexionLink">Pannel Admin</a>';
        echo '<a href="deco.php" id="deconnexionLink">Déconnexion</a>';
    } elseif(!isset($_SESSION['user_name'])) {
        echo '<a href="connexion.php" id="connexionLink">Connexion</a>';
    }
?>

</div>
        </section>
        <section id="sousMenu">
            <div class="sousMenu">
 <!-- Implentation code PHP -->
                <?php
$requete=$pdo->prepare("SELECT nom FROM categories WHERE 1");
$requete->execute();
$categories=$requete->fetchAll(PDO::FETCH_ASSOC);

foreach($categories as $nom){
                echo'<ul>
                    <li><a href="categorie.php?nom='.$nom['nom'].'">' .$nom['nom']. '</a></li>
                </ul>';
            }
                ?>
            </div>
        </section>
        <div class="popup" id="popup">
            <h2>Inscrivez-vous à notre newsletter</h2>
            <form>
                <input type="email" placeholder="Entrez votre adresse e-mail">
                <button type="submit">S'abonner</button>
            </form>
            <button id='close_popup' onclick="closePopup()">Fermer</button>
        </div>
        <div class="overlay" id="overlay"></div>
    </header>
<main>
    <h1 id='blog_titre'>Blog - Tous les articles</h1>
    <section id='tous_articles'>
        <form method="POST">
            <div class="tous_articles">
                <div class="recherche">
                <input type="search" name="search" placeholder="Votre recherche" value="<?php echo htmlspecialchars($search); ?>">
                <input type="submit" name="search_submit" value="Rechercher">
                </div>
                <div class="tri">
                <select name="tri">
                    <option value="recent" <?php if($tri === 'recent') echo 'selected'; ?>>Plus récent</option>
                    <option value="ancien" <?php if($tri === 'ancien') echo 'selected'; ?>>Plus ancien</option>
                </select>
                <input type="submit" name="trier" value="Trier">
                </div>
                </div>
        </form>
        <?php afficherArticles($articles, $pdo); ?>
    </section>    
</main>
    
<footer class="flex">
        <section id="categorie_footer flex">
            <ul class="nav_footer flex">
                <?php
                    $requete=$pdo->prepare("SELECT nom FROM categories WHERE 1");
                    $requete->execute();
                    $categories=$requete->fetchAll(PDO::FETCH_ASSOC);

                    foreach($categories as $nom){

                    echo'<li class="nav_item_footer flex">
                        <a href="categorie.php?nom='.$nom['nom'].'">' .$nom['nom']. '</a>';
                    }
                ?>
            </ul>
        </section>
        <div class="line container"></div>
        <section class="footer_down flex container">
            <section class="footer_identite flex">
                <img id="logo_footer" src="../img/logo.svg" alt="logo">
                <div class="text_identite flex">
                    <p class="p_id">LA SPHÈRE DIGITAL</p>
                    <div class="mentions flex">
                        <a href="mentions_legales.php">Mentions légales</a>
                        <p>|</p>
                        <a href="politique.php">Politiques de confidentialités</a>
                    </div>
                </div>
            </section>
            <section class="footer_contact flex">
                <div class="reseaux_size">
                    <a href="#"><img src="../img/x_blanc.svg" alt="logo x"></a>
                    <a href="#"><img src="../img/youtube_blanc.svg" alt="logo youtube"></a>
                    <a href="#"><img src="../img/insta_blanc.svg" alt="logo instagram"></a>
                </div>
                <div class="footer_contact_mail flex">
                    <img id="img_mail" src="../img/mail_blanc.svg" alt="icon de mail">
                    <p>ADRESSEPRO@MEDIASCHOOL.COM</p>
                </div>
            </section>
        </section>
    </footer>
</body>
</html>