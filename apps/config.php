<?php

$serveur="localhost"; # DSN pour la connexion PDO
$utilisateur="root"; # Nom d'utilisateur MySQL
$motDePasse="root"; # Mot de passe MySQL
$baseDeDonnees="sphere"; #Nom de la base de donnée

// ----------------------------------------------- CONNEXION BDD --------------------------------------------------------

try{
    $pdo=new PDO("mysql:host=$serveur;dbname=$baseDeDonnees",$utilisateur,$motDePasse);
    $pdo->setAttribute(PDO::ATTR_ERRMODE,PDO::ERRMODE_EXCEPTION);

    if (basename($_SERVER['SCRIPT_FILENAME']) == 'config.php') {
    echo "Connexion réussie à la base de données. <br>"; //Suppression du message lors d'un include 
    }

    }catch(PDOException $e){ 
    die("La connexion a échoué : " .$e->getMessage());
    }
?>