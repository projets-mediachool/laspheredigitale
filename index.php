<?php
 session_start();
    // Vérifiez si l'utilisateur est connecté
    if (isset($_SESSION['user_name'])) {
        $utilisateur_connecte = true;
    } else {
        $utilisateur_connecte = false;
    }
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>LSD | Accueil</title>
    <link rel="icon" type="svg" href="img/logo.svg">
    <link rel="stylesheet" href="css\index.css">
    <link rel="stylesheet" href="css\footer.css">
    <link rel="stylesheet" href="https://use.typekit.net/lmh7ctl.css">
    <script src="js/script_index.js" defer></script>
    <script src="js/script_newsletter.js" defer></script>
</head>
<body>
    <header>
        <section class="header" id="myHeader">
            <div class="logo">
                <img src="img/logo_header.svg" alt="image du logo du site">
            </div>
            <nav>
                <ul>
                    <li><a href="index.php">Accueil</a></li>
                    <li><a href="apps/a_propos.php">A propos</a></li>
                    <li id="blog"><a href="apps/blog.php">Blog</a></li>
                    <li><a href="apps/contact.php">Contact</a></li>
                </ul>
            </nav>
            <div class="CTA">
                <div class="icone">
                    <img id='newsletter' src="img/mail.svg" alt="logo mail">
                </div>
                <div class="icone">
                    <img id="connexionImage"src="img/login.svg" alt="logo connexion">
                </div>
<!-- Implentation code PHP -->
<?php
    include_once("apps/config.php");

    $requete = $pdo->prepare("SELECT user_name, admin FROM utilisateur WHERE 1");
    $requete->execute();
    $resultat = $requete->fetch(PDO::FETCH_ASSOC);

    if(isset($_SESSION['user_name']) && $resultat['admin'] == 0) {
        echo '<a href="apps/deco.php" id="deconnexionLink">Déconnexion</a>';
    } elseif(isset($_SESSION['user_name']) && $resultat['admin'] == 1) {
        echo '<a href="apps/admin.php" id="deconnexionLink">Pannel Admin</a>';
        echo '<a href="apps/deco.php" id="deconnexionLink">Déconnexion</a>';
    } elseif(!isset($_SESSION['user_name'])) {
        echo '<a href="apps/connexion.php" id="connexionLink">Connexion</a>';
    }
?>

            </div>
        </section>
        <section id="sousMenu">
            <div class="sousMenu">
 <!-- Implentation code PHP -->
                <?php
                include_once("apps/config.php");
$requete=$pdo->prepare("SELECT nom FROM categories WHERE 1");
$requete->execute();
$categories=$requete->fetchAll(PDO::FETCH_ASSOC);

foreach($categories as $nom){
                echo'<ul>
                    <li><a href="apps/categorie.php?nom='.$nom['nom'].'">' .$nom['nom']. '</a></li>
                </ul>';
            }
                ?>
            </div>
        </section>
        <div class="popup" id="popup">
            <h2>Inscrivez-vous à notre newsletter</h2>
            <form>
                <input type="email" placeholder="Entrez votre adresse e-mail">
                <button type="submit">S'abonner</button>
            </form>
            <button id='close_popup' onclick="closePopup()">Fermer</button>
        </div>
        <div class="overlay" id="overlay"></div>
    </header>
    <main>
        <section class="slider">           
                <div class="slider_nav">
                    <button onclick="previous()" class="previousBtn"><img src="img/fleche_gauche.svg" alt="bouton fleche gauche"></button>
                    <button onclick="next()" class="nextBtn"><img src="img/fleche_droite.svg" alt="bouton fleche droite"></button>
                </div>
                <div class="slider_content">
                    <div class="slider_content_item">
                        <img src="img/Slider2.jpg" style="width:100%;" alt="Image 1">
                    </div>
                    <div class="slider_content_item">
                        <img src="img/Slider1.jpg" style="width:100%;" alt="Image 2">
                    </div>
                    <div class="slider_content_item">
                        <img src="img/Slider3.jpg" style="width:100%;" alt="Image 3">
                    </div>
                </div>
        </section>
        <section class="introduction">
            <div class="intro_container">
                <div class="intro_text">
                    <h2>Présentation de la Sphère Digitale</h2>
                    <h3>Nous sommes un blog recensant de nombreux articles à propos du numérique</h3>
                    <p>Dans ce site, vous retrouverez différentes catégories dans lesquelles nos journalistes écrivent sur des sujets allant du développement web au marketing digital</p>
                </div>
                <div class="intro_img">
                    <img src="img/ilustration_blog.svg" alt="image ordinateur">
                </div>
            </div>
        </section>
        <section class="categories">
            <h2>Découvrez nos catégories</h2>
            <div class="wiper">
                <button class="wiper-button wiper-button__right">
                    <img src="https://www.iconpacks.net/icons/2/free-arrow-left-icon-3099-thumb.png" alt="left" />
                </button>
                <div class="wiper-wrapper">
                    <ul class="wiper-track">
                        <li class="wiper-item">
                            <img src="img/icon_tech.svg" alt="icone d'ampoule">
                            <p></p>
                        </li>
                        <li class="wiper-item active-swipe">
                            <img src="img/icon_dev.svg" alt="icone d'ampoule">
                            <p></p>
                        </li>
                        <li class="wiper-item">
                            <img src="img/icon_design.svg" alt="icone d'ampoule">
                            <p></p>
                        </li>
                        <li class="wiper-item">
                            <img src="img/icon_marketing.svg" alt="icone d'ampoule">
                            <p></p>
                        </li>
                        <li class="wiper-item">
                            <img src="img/icon_ADC.svg" alt="icone d'ampoule">
                            <p></p>
                        </li>
                    </ul>
                </div>
                <button class="wiper-button wiper-button__left">
                    <img src="https://www.iconpacks.net/icons/2/free-arrow-left-icon-3099-thumb.png" alt="right" />
                </button>
            </div>
        </section>
        <section id="equipe">
            <h2>A propos de nous</h2>
            <div class="equipe">
                <div id="ewen">
                    <img src="img/ewen.png" alt="photo d'ewen">
                    <p>Ewen :<br> Specialiste Web Design</p>
                </div>
                <div id="dorine">
                    <img src="img/dorine.png" alt="photo d'ewen">
                    <p>Dorine :<br> Chercheuse de nouvelles technologies</p>
                </div>
                <div id="alex">
                    <img src="img/alex.png" alt="photo d'alex">
                    <p>Alexandre :<br> Expert en stratégie marketing</p>
                </div>
            </div>
            <button class="savoir_plus"><a href="a_propos.php">En savoir +</a></button>
        </section>
        <div id='hr'><hr></div>
        <section id="temoignage">
            <h2>TEMOIGNAGES</h2>
            <h3>MOTS DE NOS LECTEURS</h3>
            <div class="temoignage">
                <div class="com1">
                    <p>Depuis que j'ai découvert La Sphère Digitale, ma perspective sur le monde du digital a complètement changé. Les articles sont non seulement informatifs, mais aussi incroyablement captivants. C'est devenu ma référence principale pour rester à jour sur les dernières tendances en matière de technologie et de numérique.</p>
                    <h4>MANON HASSAN</h4>
                    <h5>Lectrice confirmée</h5>
                    <img src="img/quote.svg" alt="">
                </div>
                <div class="com2">
                    <p>Ce que j'apprécie le plus chez La Sphère Digitale, c'est la diversité des sujets abordés et la qualité de la rédaction. Que vous soyez un débutant ou un expert en digital, ce blog a quelque chose à offrir à tout le monde. Une ressource précieuse pour quiconque cherche à approfondir ses connaissances numériques.</p>
                    <h4>JEROME PIAUT</h4>
                    <h5>Lecteur confirmé</h5>
                    <img src="img/quote.svg" alt="">
                </div>
            </div>
        </section>
    </main>
    <footer class="flex">
        <section id="categorie_footer flex">
            <ul class="nav_footer flex">

            <?php
                include_once("apps/config.php");
$requete=$pdo->prepare("SELECT nom FROM categories WHERE 1");
$requete->execute();
$categories=$requete->fetchAll(PDO::FETCH_ASSOC);

foreach($categories as $nom){

                echo'<li class="nav_item_footer flex">
                    <a href="apps/categorie.php?nom='.$nom['nom'].'">' .$nom['nom']. '</a>';
                }
            ?>

            </ul>
        </section>
        <div class="line container"></div>
        <section class="footer_down flex container">
            <section class="footer_identite flex">
                <img id="logo_footer" src="img/logo.svg" alt="logo">
                <div class="text_identite flex">
                    <p class="p_id">LA SPHÈRE DIGITAL</p>
                    <div class="mentions flex">
                        <a href="apps/mentions_legales.php">Mentions légales</a>
                        <p>|</p>
                        <a href="apps/politique.php">Politiques de confidentialités</a>
                    </div>
                </div>
            </section>
            <section class="footer_contact flex">
                <div class="reseaux_size">
                    <a href="#"><img src="img/x_blanc.svg" alt="logo x"></a>
                    <a href="#"><img src="img/youtube_blanc.svg" alt="logo youtube"></a>
                    <a href="#"><img src="img/insta_blanc.svg" alt="logo instagram"></a>
                </div>
                <div class="footer_contact_mail flex">
                    <img id="img_mail" src="img/mail_blanc.svg" alt="icon de mail">
                    <p>ADRESSEPRO@MEDIASCHOOL.COM</p>
                </div>
            </section>
        </section>
    </footer>
    <script>
        document.addEventListener('DOMContentLoaded', function() {
            var connexionImage = document.getElementById('connexionImage');
            var connexionLink = document.getElementById('connexionLink');
            var deconnexionLink = document.getElementById('deconnexionLink');

            <?php if ($utilisateur_connecte): ?>
                connexionImage.addEventListener('click', function() {
                    // Afficher le lien de déconnexion
                    deconnexionLink.style.display = 'inline';
                    // Masquer le lien de connexion
                    connexionLink.style.display = 'none';
                });
            <?php else: ?>
                connexionImage.addEventListener('click', function() {
                    // Afficher le lien de connexion
                    connexionLink.style.display = 'inline';
                    // Masquer le lien de déconnexion
                    deconnexionLink.style.display = 'none';
                });
            <?php endif; ?>
        });
    </script>
</body>
</html>