// Fonction pour afficher la pop-up
function openPopup() {
    document.getElementById("popup").style.display = "block";
    document.getElementById("overlay").style.display = "block";
  }

  // Fonction pour fermer la pop-up
  function closePopup() {
    document.getElementById("popup").style.display = "none";
    document.getElementById("overlay").style.display = "none";
  }

  // Événement de clic sur l'image pour afficher la pop-up
  var newsletter = document.getElementById("newsletter");
  newsletter.addEventListener("click", function(event) {
    openPopup();
    event.preventDefault(); // Pour éviter le comportement par défaut du lien
  });
