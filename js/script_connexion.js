document.addEventListener('DOMContentLoaded', function() {
    var inscription_btns = document.querySelectorAll('.inscription_btn');
    var connexion_btns = document.querySelectorAll('.connexion_btn');
    var inscription_btn = document.querySelectorAll('#inscription_btn');
    var connexion_btn = document.querySelectorAll('#connexion_btn');
    var inscription_div = document.getElementById('inscription');
    var connexion_div = document.getElementById('connexion');

    inscription_btns.forEach(function(btn) {
        btn.addEventListener('click', function() {
            // Cacher la div connexion_container
            connexion_div.style.display = 'none';
            // Afficher la div inscription_container
            inscription_div.style.display = 'block';
        });
    });

    connexion_btns.forEach(function(btn) {
        btn.addEventListener('click', function() {
            // Cacher la div inscription_container
            inscription_div.style.display = 'none';
            // Afficher la div connexion_container
            connexion_div.style.display = 'block';
        });
    });
    inscription_btn.forEach(function(btn) {
        btn.addEventListener('click', function() {
            // Cacher la div connexion_container
            connexion_div.style.display = 'none';
            // Afficher la div inscription_container
            inscription_div.style.display = 'block';
        });
    });

    connexion_btn.forEach(function(btn) {
        btn.addEventListener('click', function() {
            // Cacher la div inscription_container
            inscription_div.style.display = 'none';
            // Afficher la div connexion_container
            connexion_div.style.display = 'block';
        });
    });
});