// JS POUR LA PARTIE HEADER

  // Quand l'utilisateur scroll, la fonction de sticky bar est executée
    window.onscroll = function() {myFunction()};

    var header = document.getElementById("myHeader");
    var sticky = header.offsetTop;

    function myFunction() {
      if (window.scrollY > sticky) {
        header.classList.add("sticky");
      } else {
        header.classList.remove("sticky");
      }
    }
  // Créer un sous-menu au survol du mot blog
      const blog = document.querySelector("#blog");
      const sousMenu = document.querySelector("#sousMenu");

      // Afficher le sous-menu lorsque survolé le texte "Blog"
      blog.addEventListener("mouseover", () => {
          sousMenu.style.display = "block";
      });

      // Cacher le sous-menu lorsque la souris quitte le texte "Blog" ou le sous-menu lui-même
      blog.addEventListener("mouseout", () => {
          sousMenu.style.display = "block";
      });

      sousMenu.addEventListener("mouseover", () => {
          sousMenu.style.display = "block"; // Maintenir le sous-menu affiché lorsque survolé
      });

      sousMenu.addEventListener("mouseout", () => {
          sousMenu.style.display = "none"; // Cacher le sous-menu lorsque la souris quitte le sous-menu
      });
// JS POUR LA PARTIE SLIDER
  function previous() {
    const widthSlider = document.querySelector('.slider').offsetWidth;
    document.querySelector('.slider_content').scrollLeft -= widthSlider;
  }
  function next() {
    const widthSlider = document.querySelector('.slider').offsetWidth;
    const sliderContent = document.querySelector('.slider_content');
    sliderContent.scrollLeft += widthSlider;
    const scrollLeft = sliderContent.scrollLeft;
    const itemSlider = document.querySelectorAll('.slider_content_item');
  }

// JS pour la partie carroussel categorie

  const wiperTrack = document.querySelector(".wiper-track");
  const wipes = Array.from(wiperTrack.children);
  const wipePrevBtn = document.querySelector(".wiper-button__right");
  const wipeNextBtn = document.querySelector(".wiper-button__left");
  const wipeWidth = wipes[0].getBoundingClientRect().width;

  const arrowsBehaviour = (wipePrevBtn, wipeNextBtn, index) => {
    if (index === 0) {
      wipePrevBtn.classList.add("is-hidden");
      wipeNextBtn.classList.remove("is-hidden");
    } else if (index === wipes.length-1) {
      wipePrevBtn.classList.remove("is-hidden");
      wipeNextBtn.classList.add("is-hidden");
    } else {
      wipePrevBtn.classList.remove("is-hidden");
      wipeNextBtn.classList.remove("is-hidden");
    }
  };

  const wipeSlide = (wiperTrack, activeSlide, nextSlide, targetIndex) => {
    wiperTrack.style.transform =
      "translateX(-" + (wipeWidth + 24) * (targetIndex - 1) + "px)";
    activeSlide.classList.remove("active-swipe");
    activeSlide.style.transform = "scale(1)";
    nextSlide.classList.add("active-swipe");
    nextSlide.style.transform = "scale(1.1)";
  };

  wipeNextBtn.addEventListener("click", (e) => {
    const activeSlide = wiperTrack.querySelector(".active-swipe");
    const nextSlide = activeSlide.nextElementSibling;
    const targetIndex = wipes.findIndex((slide) => slide === nextSlide);
    wipeSlide(wiperTrack, activeSlide, nextSlide, targetIndex);
    arrowsBehaviour(wipePrevBtn, wipeNextBtn, targetIndex);
  });
  wipePrevBtn.addEventListener("click", (e) => {
    const activeSlide = wiperTrack.querySelector(".active-swipe");
    const nextSlide = activeSlide.previousElementSibling;
    const targetIndex = wipes.findIndex((slide) => slide === nextSlide);
    wipeSlide(wiperTrack, activeSlide, nextSlide, targetIndex);
    arrowsBehaviour(wipePrevBtn, wipeNextBtn, targetIndex);
  });